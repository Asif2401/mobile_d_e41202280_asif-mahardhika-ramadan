void main(List<String> args) async {
  print("Ready? Sing!");
  await line1();
  await line2();
  await line3();
  await line4();
  await line5();
  await line6();
}

Future<void> line1() async {
  String message = "Tolong....";
  final duration = Duration(seconds: 2);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line2() async {
  String message = "Katakan pada dirinya...";
  final duration = Duration(seconds: 5);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line3() async {
  String message = "Lagu ini kutuliskan untuknya...";
  final duration = Duration(seconds: 8);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line4() async {
  String message = "Namanya slalu kusebut dalam doa";
  final duration = Duration(seconds: 6);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line5() async {
  String message = "Sampai aku mampu...";
  final duration = Duration(seconds: 3);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line6() async {
  String message = "Ucap mau kah denganku...";
  final duration = Duration(seconds: 6);
  return await Future.delayed(duration, () => print(message));
}
